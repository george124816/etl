# This file is responsible for configuring your application
# and its dependencies with the aid of the Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
import Config

# Adapters 
config :etl, Etl.Adapters.Extract, url: "http://challenge.dienekes.com.br/api/numbers?page="

# Services
config :etl, Etl.Services.Extract, adapter: Etl.Adapters.Extract

# Tesla
config :tesla, adapter: Tesla.Adapter.Hackney

# Repo
config :etl,
  ecto_repos: [Etl.Repo]

# Oban
config :etl, Oban,
  repo: Etl.Repo,
  plugins: [
    {Oban.Plugins.Cron,
     crontab: [
       {"0 * * * *", Etl.Jobs.UpdateJob}
     ]}
  ],
  queues: [default: 10, events: 50, media: 20]

# Configures the endpoint
config :etl, EtlWeb.Endpoint,
  url: [host: "localhost"],
  render_errors: [view: EtlWeb.ErrorView, accepts: ~w(json), layout: false],
  pubsub_server: Etl.PubSub,
  live_view: [signing_salt: "fyMud7f7"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{config_env()}.exs"
