import Config

# Adapters 
config :etl, Etl.Adapters.Extract, url: "http://challenge.dienekes.com.br/api/numbers?page="

# Services
config :etl, Etl.Services.Extract, adapter: Etl.Adapters.Extract

# Tesla Mock
config :tesla, adapter: Tesla.Mock

config :tesla, Etl.Adapters.Extract, adapter: Tesla.Mock

# Repo
config :etl, Etl.Repo,
  username: "postgres",
  password: "postgres",
  hostname: "localhost",
  database: "etl_test#{System.get_env("MIX_TEST_PARTITION")}",
  pool: Ecto.Adapters.SQL.Sandbox,
  pool_size: 10

# Oban
config :etl, Oban,
  plugins: false,
  queues: false

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :etl, EtlWeb.Endpoint,
  http: [ip: {127, 0, 0, 1}, port: 4002],
  secret_key_base: "8SiP9qWOx/ORo9Ahb/F+yWo2ZtLrs0iUTVyOzd69fdsAldF/IwDQwovTBV7/dD0m",
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Initialize plugs at runtime for faster test compilation
config :phoenix, :plug_init_mode, :runtime
