defmodule Etl.Repo.Migrations.CreateNumbersTable do
  use Ecto.Migration

  def change do
    create table(:numbers, primary_key: false) do
      add(:id, :uuid)
      add(:values, {:array, :float})
      timestamps(updated_at: false)
    end
  end
end
