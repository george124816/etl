Mox.defmock(ServiceExtractMock, for: Etl.ServicesBehaviour)
Application.put_env(:etl, Etl.Services.Extract, adapter: ServiceExtractMock)

ExUnit.start()
Ecto.Adapters.SQL.Sandbox.mode(Etl.Repo, :manual)
