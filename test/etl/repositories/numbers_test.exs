defmodule Etl.Repositories.NumbersTest do
  use Etl.DataCase

  alias Etl.Repo
  alias Etl.Models.Number
  alias Etl.Repositories.Numbers

  describe "get_last_numbers" do
    test "should return last numbers" do
      numbers_one = Enum.map(1..500, fn _ -> :rand.uniform() end)
      numbers_two = Enum.map(1..500, fn _ -> :rand.uniform() end)
      now = NaiveDateTime.truncate(NaiveDateTime.utc_now(), :second)
      past = NaiveDateTime.add(now, -100)

      %Number{values: numbers_one, inserted_at: past}
      |> Repo.insert!()

      %Number{values: numbers_two, inserted_at: now}
      |> Repo.insert!()

      assert {:ok, %Number{values: ^numbers_two}} = Numbers.get_last_numbers()
    end

    test "should return error when doesn't have numbers" do
      assert {:error, :not_found} == Numbers.get_last_numbers()
    end
  end
end
