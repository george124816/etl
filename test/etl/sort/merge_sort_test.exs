defmodule Etl.Sort.MergeSortTest do
  use ExUnit.Case

  import Etl.Sort.MergeSort

  describe "sort/1" do
    test "should sort small quantity of numbers successfully" do
      list =
        Enum.map(1..10, fn _ ->
          :rand.uniform()
        end)

      expected_list = Enum.sort(list)

      assert expected_list == sort(list)
    end

    test "should sort large quantity of numbers successfully" do
      list =
        Enum.map(1..20_000, fn _ ->
          :rand.uniform()
        end)

      expected_list = Enum.sort(list)

      assert expected_list == sort(list)
    end
  end
end
