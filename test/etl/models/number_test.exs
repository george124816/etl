defmodule Etl.Models.NumberTest do
  use Etl.DataCase

  alias Etl.Models.Number

  describe "changeset/1" do
    test "should return invalid changeset when doesn't required fields" do
      params = %{}

      assert changeset = Number.changeset(params)
      assert errors_on(changeset) == %{values: ["can't be blank"]}
    end

    test "should return valid changeset when have valid params" do
      params = %{values: ["3", "1", "3"]}

      assert changeset = Number.changeset(params)
      assert errors_on(changeset) == %{}
    end

    test "should insert a valid changeset" do
      values =
        Enum.map(1..100, fn _ ->
          :rand.uniform()
        end)

      params = %{values: values}

      assert changeset = Number.changeset(params)
      assert {:ok, %Number{values: ^values}} = Repo.insert(changeset)
    end
  end
end
