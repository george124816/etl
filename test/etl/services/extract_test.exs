defmodule Etl.Services.ExtractTest do
  use ExUnit.Case

  import Mox

  alias Etl.Services.Extract

  describe "get_data/1" do
    test "should return ok when adapter return ok" do
      expected_numbers =
        Enum.map(1..100, fn _ ->
          Enum.random(1..1000)
        end)

      expect(ServiceExtractMock, :get_data, fn _ ->
        {:ok, expected_numbers}
      end)

      assert {:ok, expected_numbers} == Extract.get_data(5)
    end

    test "should return error when adapter return interna server error" do
      expect(ServiceExtractMock, :get_data, fn _ ->
        {:error, "Simulated internal error"}
      end)

      assert {:error, "Simulated internal error"} == Extract.get_data(5)
    end

    test "should return error when request failed" do
      expect(ServiceExtractMock, :get_data, fn _ ->
        {:error, "not_found"}
      end)

      assert {:error, "not_found"} == Extract.get_data(5)
    end
  end
end
