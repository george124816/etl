defmodule Etl.ExtractTest do
  use Etl.DataCase

  import Mox

  alias Etl.Extract
  alias Etl.Models.Number

  describe "get_data/1" do
    test "should return 150 pages" do
      expected_numbers =
        Enum.map(1..150, fn _ ->
          Enum.map(1..100, fn _ -> :rand.uniform() end)
        end)

      expected_sorted_numbers = expected_numbers |> List.flatten() |> Enum.sort()

      expect(ServiceExtractMock, :get_data, 151, fn
        151 ->
          {:ok, []}

        n ->
          result = Enum.at(expected_numbers, n - 1)
          {:ok, result}
      end)

      assert {:ok, %Number{values: result}} = Extract.get_data()
      assert result == expected_sorted_numbers
    end
  end
end
