defmodule Etl.Jobs.UpdateJobTest do
  use Etl.DataCase
  use Oban.Testing, repo: Etl.Repo

  alias Etl.Jobs.UpdateJob

  describe "perform/1" do
    test "should enqueue job successfully" do
      Oban.insert!(UpdateJob.new(%{}))

      assert_enqueued(worker: "Etl.Jobs.UpdateJob")
    end
  end
end
