defmodule Etl.Adapters.ExtractTest do
  use ExUnit.Case

  import ExUnit.CaptureLog

  alias Etl.Adapters.Extract

  describe "get_data/1" do
    test "should return data when endpoint returns successfully" do
      expected_numbers =
        Enum.map(1..100, fn _ ->
          Enum.random(1..1000)
        end)

      Tesla.Mock.mock(fn _ ->
        %Tesla.Env{
          body: Jason.encode!(%{"numbers" => expected_numbers})
        }
      end)

      assert {:ok, expected_numbers} == Extract.get_data(5)
    end

    test "should return error when :internal_server_error" do
      Tesla.Mock.mock(fn _ ->
        %Tesla.Env{body: Jason.encode!(%{"error" => "Simulated internal error"})}
      end)

      assert {result, log} =
               with_log(fn ->
                 Extract.get_data(5)
               end)

      assert {:error, "Simulated internal error"} == result
      assert log =~ ~s/Elixir.Etl.Adapters.Extract.handle_result error="Simulated internal error"/
    end

    test "should return error request failed" do
      Tesla.Mock.mock(fn _ ->
        %Tesla.Env{status: 404, body: "not_found"}
      end)

      assert {result, log} =
               with_log(fn ->
                 Extract.get_data(5)
               end)

      assert {:error, "Failed to parse"} == result
      assert log =~ ~s/Elixir.Etl.Adapters.Extract.handle_result error=failed to parse/
    end
  end
end
