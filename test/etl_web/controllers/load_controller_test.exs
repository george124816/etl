defmodule EtlWeb.LoadControllerTest do
  use EtlWeb.ConnCase

  alias Etl.Models.Number

  describe "/api" do
    test "should return error when doesn't have data yet", %{conn: conn} do
      result =
        conn
        |> get(Routes.load_path(conn, :load))
        |> json_response(404)

      assert %{"error" => "extracting data, wait."} == result
    end

    test "should return numbers when already extracted data", %{conn: conn} do
      numbers = Enum.map(1..100, fn _ -> :rand.uniform() end)
      now = NaiveDateTime.truncate(NaiveDateTime.utc_now(), :second)

      %Number{values: numbers, inserted_at: now}
      |> Etl.Repo.insert!()

      expected_inserted_at = NaiveDateTime.to_iso8601(now)

      result =
        conn
        |> get(Routes.load_path(conn, :load))
        |> json_response(200)

      assert %{"numbers" => numbers, "inserted_at" => expected_inserted_at} == result
    end
  end
end
