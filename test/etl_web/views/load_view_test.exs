defmodule EtlWeb.LoadViewTest do
  use ExUnit.Case

  alias EtlWeb.LoadView

  describe "render/1" do
    test "should render successfully" do
      values = Enum.map(1..500, fn _ -> :rand.uniform() end)
      inserted_at = NaiveDateTime.utc_now()

      numbers = %{
        values: values,
        inserted_at: inserted_at
      }

      assert %{
               numbers: values,
               inserted_at: inserted_at
             } ==
               LoadView.render("numbers.json", %{numbers: numbers})
    end

    test "should render error clause" do
      reason = "not_found"
      params = %{error: reason}

      assert %{error: reason} == LoadView.render("numbers.json", params)
    end

    test "should return raise when doesn't match params type" do
      assert_raise(Phoenix.Template.UndefinedError, fn -> LoadView.render("numbers.json", []) end)

      assert_raise(Phoenix.Template.UndefinedError, fn -> LoadView.render("numbers.json", %{}) end)

      assert_raise(Protocol.UndefinedError, fn -> LoadView.render("numbers.json", nil) end)

      assert_raise(Protocol.UndefinedError, fn -> LoadView.render("numbers.json", 5) end)
    end
  end
end
