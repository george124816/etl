defmodule Etl.Extract do
  @moduledoc """
  A module that call service to retrieve numbers information.
  """
  require Logger

  alias Etl.Models.Number
  alias Etl.Repo
  alias Etl.Services.Extract
  alias Etl.Sort

  @spec get_data ::
          {:ok, Number.t()} | {:error, Ecto.Changeset.t()}
  def get_data do
    extract()
    |> transform()
  end

  defp extract(accumulated \\ [], n \\ 1) do
    n
    |> Extract.get_data()
    |> case do
      {:ok, []} ->
        Logger.info("#{__MODULE__}.do_get_data extract finished")
        accumulated

      {:ok, [_ | _] = data} ->
        extract(data ++ accumulated, n + 1)

      {:error, reason} ->
        Logger.error("#{__MODULE__}.do_get_data reason=#{inspect(reason)}")
        extract(accumulated, n + 1)
    end
  end

  defp transform(data) do
    sorted_data = Sort.MergeSort.sort(data)

    %{
      values: sorted_data
    }
    |> Number.changeset()
    |> Repo.insert()
  end
end
