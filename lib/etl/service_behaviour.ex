defmodule Etl.ServicesBehaviour do
  @moduledoc """
  A behaviour that services needs to implement
  """
  @callback get_data(integer()) :: {:ok, any()} | {:error, String.t()}
end
