defmodule Etl.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      # Start Repo
      Etl.Repo,
      # Start the Telemetry supervisor
      EtlWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: Etl.PubSub},
      # Start the Endpoint (http/https)
      EtlWeb.Endpoint,
      # Start a worker by calling: Etl.Worker.start_link(arg)
      # {Etl.Worker, arg}
      {Oban, oban_config()},
      {Etl.Tasks.RunAfterInit, []}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Etl.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    EtlWeb.Endpoint.config_change(changed, removed)
    :ok
  end

  defp oban_config do
    Application.fetch_env!(:etl, Oban)
  end
end
