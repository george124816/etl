defmodule Etl.Services.Extract do
  @moduledoc """
  A service that implement a default service behaviour
  """
  @behaviour Etl.ServicesBehaviour

  def get_data(page) do
    case adapter().get_data(page) do
      {:ok, result} ->
        {:ok, result}

      {:error, reason} ->
        {:error, reason}
    end
  end

  defp adapter do
    :etl
    |> Application.fetch_env!(Etl.Services.Extract)
    |> Keyword.fetch!(:adapter)
  end
end
