defmodule Etl.Sort.MergeSort do
  @moduledoc """
  MergeSort
  """
  def sort([list]), do: [list]

  def sort(list) do
    {head, tail} = Enum.split(list, list |> length() |> div(2))
    :lists.merge(sort(head), sort(tail))
    # merge(sort(head), sort(tail))
  end

  # def merge([], []), do: []
  # def merge([], tail), do: tail
  # def merge(head, []), do: head

  # def merge([head1 | tail1], [head2 | tail2]) when head1 < head2 do
  #   [head1 | merge(tail1, [head2 | tail2])]
  # end

  # def merge([head1 | tail1], [head2 | tail2]) when head1 > head2 do
  #   [head1 | merge(tail1, [head2 | tail2])]
  #   [head2 | merge([head1 | tail1], tail2)]
  # end
end
