defmodule Etl.Sort.BubbleSort do
  @moduledoc """
  BubbleSort
  """
  def sort(list) when is_list(list) do
    interactions = length(list)

    Enum.reduce(1..interactions, [], fn
      _, acc when acc == [] ->
        do_sort_interaction(list)

      _, acc ->
        do_sort_interaction(acc)
    end)
  end

  defp do_sort_interaction([x, y | tail]) when x > y do
    [y | do_sort_interaction([x | tail])]
  end

  defp do_sort_interaction([x, y | tail]) do
    [x | do_sort_interaction([y | tail])]
  end

  defp do_sort_interaction(list) do
    list
  end
end
