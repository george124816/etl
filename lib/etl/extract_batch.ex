defmodule Etl.ExtractBatch do
  @moduledoc """
  A module that call service to retrieve numbers information
  using batch strategy.
  """
  require Logger

  alias Etl.Models.Number
  alias Etl.Repo
  alias Etl.Services.Extract
  alias Etl.Sort

  @batch_size 50

  @spec get_data ::
          {:ok, Number.t()} | {:error, Ecto.Changeset.t()}
  def get_data do
    extract_batch()
    |> transform()
  end

  defp extract_batch(accumulated \\ [], n \\ 1) do
    n
    |> get_batch_data()
    |> case do
      {:ok, []} ->
        Logger.info("#{__MODULE__}.do_get_data extract finished")
        accumulated

      {:ok, [_ | _] = data} ->
        extract_batch(data ++ accumulated, n + @batch_size)
    end
  end

  defp get_batch_data(size) do
    final = size + @batch_size

    size..final
    |> Task.async_stream(&Extract.get_data/1)
    |> Enum.into([], fn {:ok, res} -> res end)
    |> Enum.map(fn
      {:error, _reason} ->
        []

      {:ok, res} ->
        res
    end)
    |> List.flatten()
    |> case do
      [] ->
        {:ok, []}

      [_ | _] = numbers ->
        {:ok, numbers}
    end
  end

  defp transform(data) do
    sorted_data = Sort.MergeSort.sort(data)

    %{
      values: sorted_data
    }
    |> Number.changeset()
    |> Repo.insert()
  end
end
