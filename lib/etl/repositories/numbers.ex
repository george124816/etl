defmodule Etl.Repositories.Numbers do
  @moduledoc """
  A repository to retrieve a numbers.
  """
  import Ecto.Query

  alias Etl.Repo
  alias Etl.Models.Number

  def get_last_numbers do
    Number
    |> order_by(desc: :inserted_at)
    |> limit(1)
    |> Repo.all()
    |> case do
      [number] ->
        {:ok, number}

      [] ->
        {:error, :not_found}
    end
  end
end
