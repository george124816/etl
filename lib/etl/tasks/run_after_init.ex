defmodule Etl.Tasks.RunAfterInit do
  @moduledoc """
  A task to run after start a server.
  """
  use GenServer

  def start_link(args) do
    GenServer.start_link(__MODULE__, args, name: __MODULE__)
  end

  @impl true
  def init(_stack) do
    Etl.Tasks.Release.migrate()
    Oban.insert!(Etl.Jobs.UpdateJob.new(%{}))

    {:ok, []}
  end
end
