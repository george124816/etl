defmodule Etl.Models.Number do
  @moduledoc """
  A Number model to represent database schema.
  """
  use Ecto.Schema

  @type t() :: %__MODULE__{}

  import Ecto.Changeset

  @primary_key {:id, Ecto.UUID, autogenerate: true}
  schema "numbers" do
    field(:values, {:array, :float})

    timestamps(updated_at: false)
  end

  def changeset(params) do
    %__MODULE__{}
    |> cast(params, [:values])
    |> validate_required([:values])
  end
end
