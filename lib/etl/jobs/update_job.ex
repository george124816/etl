defmodule Etl.Jobs.UpdateJob do
  @moduledoc """
  A Job to run Extract.
  """
  use Oban.Worker,
    queue: :default,
    priority: 1,
    max_attempts: 1

  # alias Etl.Extract
  alias Etl.ExtractBatch

  @impl Oban.Worker
  def perform(_) do
    # Extract.get_data()
    ExtractBatch.get_data()
  end
end
