defmodule Etl.Adapters.Extract do
  @moduledoc """
  A adapter module that make requests.
  """

  require Logger

  def get_data(n) do
    Logger.info("#{__MODULE__}.get_data page=#{n}")

    get_url_with_page(n)
    |> Tesla.get()
    |> case do
      {:ok, %{body: body}} ->
        body
        |> Jason.decode()
        |> handle_result()

      {:error, reason} ->
        {:ok, reason}
    end
  end

  defp handle_result({:ok, %{"numbers" => data}}) do
    Logger.info("#{__MODULE__}.handle_result successfully")
    {:ok, data}
  end

  defp handle_result({:ok, %{"error" => reason}}) do
    Logger.error("#{__MODULE__}.handle_result error=#{inspect(reason)}")
    {:error, reason}
  end

  defp handle_result(_) do
    Logger.error("#{__MODULE__}.handle_result error=failed to parse")
    {:error, "Failed to parse"}
  end

  defp get_url do
    :etl
    |> Application.fetch_env!(Etl.Adapters.Extract)
    |> Keyword.fetch!(:url)
  end

  defp get_url_with_page(n) when is_integer(n), do: get_url() <> Integer.to_string(n)
  defp get_url_with_page(n) when is_binary(n), do: get_url() <> n
end
