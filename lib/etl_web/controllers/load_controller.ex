defmodule EtlWeb.LoadController do
  use EtlWeb, :controller

  alias Etl.Repositories.Numbers

  def load(conn, _params) do
    Numbers.get_last_numbers()
    |> case do
      {:ok, numbers} ->
        render(conn, "numbers.json", %{numbers: numbers})

      {:error, :not_found} ->
        conn
        |> put_status(:not_found)
        |> render("numbers.json", %{error: "extracting data, wait."})
    end
  end
end
