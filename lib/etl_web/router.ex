defmodule EtlWeb.Router do
  use EtlWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", EtlWeb do
    pipe_through :api

    get "/", LoadController, :load
  end
end
