defmodule EtlWeb.LoadView do
  use EtlWeb, :view

  def render("numbers.json", %{numbers: numbers}) do
    %{
      inserted_at: numbers.inserted_at,
      numbers: numbers.values
    }
  end

  def render("numbers.json", %{error: reason}) do
    %{
      error: reason
    }
  end
end
